# ANSI Colours
A high-level text formatting library, supporting basic colours and attributes. The library includes a clean extension model for other ANSI escape sequences, if they are needed.

More information:
* [Wikipedia: ANSI escape code][wiki-ansi-escape-code]

This code is intentionally object-oriented. OO-code in this context is unnecessary, and less performant than a procedural or native version. However, it does demonstrate a level of clarity and hierarchy which allows a student to understand how escape sequences work (and play around with them) much better than looking at lower-level code would.

## Usage
### Format a string
```java
AnsiFormatter.format("Hello, ANSI!", ForegroundColour.GREEN, BackgroundColour.BLACK, Attribute.BOLD);
```

Returns:
```java
"\\u001B[1;40;32mHello, ANSI!\\u001B[0m"
```

### Print a formatted string to `System.out`
```java
AnsiPrinter.println("Hello, ANSI!", ForegroundColour.GREEN, BackgroundColour.BLACK, Attribute.BOLD);
```

Prints:

![Example Usage](docs/example-usage.png?raw=true)

[wiki-ansi-escape-code]: https://en.wikipedia.org/wiki/ANSI_escape_code
