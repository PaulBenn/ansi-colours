package uk.co.paulbenn.ansicolours;

import uk.co.paulbenn.ansicolours.code.Attribute;
import uk.co.paulbenn.ansicolours.code.BackgroundColour;
import uk.co.paulbenn.ansicolours.code.ForegroundColour;

public class AnsiFormatter {

    public static String format(String s, ForegroundColour foreground) {
        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setForegroundColour(foreground)
                .build()
        );
    }

    public static String format(String s, ForegroundColour foreground, Attribute... attributes) {
        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setForegroundColour(foreground)
                .setAttributes(attributes)
                .build()
        );
    }

    public static String format(String s, ForegroundColour foreground, BackgroundColour background) {
        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setForegroundColour(foreground)
                .setBackgroundColour(background)
                .build()
        );
    }

    public static String format(String s, ForegroundColour foreground, BackgroundColour background, Attribute... attributes) {
        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setForegroundColour(foreground)
                .setBackgroundColour(background)
                .setAttributes(attributes)
                .build()
        );
    }

    public static String format(String s, BackgroundColour background) {
        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setBackgroundColour(background)
                .build()
        );
    }

    public static String format(String s, BackgroundColour background, Attribute... attributes) {
        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setBackgroundColour(background)
                .setAttributes(attributes)
                .build()
        );
    }

    public static String format(String s, Attribute... attributes) {
        if (attributes == null || attributes.length == 0) {
            return s;
        }

        return applyRendition(
            s,
            SelectGraphicRendition
                .builder()
                .setAttributes(attributes)
                .build()
        );
    }

    private static String applyRendition(String s, SelectGraphicRendition rendition) {
        if (s == null) {
            return null;
        }

        if (s.isBlank()) {
            return s;
        }

        return rendition.toString() + s + SelectGraphicRendition.RESET;
    }
}
