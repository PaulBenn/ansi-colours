package uk.co.paulbenn.ansicolours;

import uk.co.paulbenn.ansicolours.code.Attribute;
import uk.co.paulbenn.ansicolours.code.BackgroundColour;
import uk.co.paulbenn.ansicolours.code.ForegroundColour;

public class AnsiPrinter {

    public static void println(String s, ForegroundColour foreground) {
        System.out.println(AnsiFormatter.format(s, foreground));
    }

    public static void println(String s, ForegroundColour foreground, Attribute... attributes) {
        System.out.println(AnsiFormatter.format(s, foreground, attributes));
    }

    public static void println(String s, ForegroundColour foreground, BackgroundColour background) {
        System.out.println(AnsiFormatter.format(s, foreground, background));
    }

    public static void println(String s, ForegroundColour foreground, BackgroundColour background, Attribute... attributes) {
        System.out.println(AnsiFormatter.format(s, foreground, background, attributes));
    }

    public static void println(String s, BackgroundColour background) {
        System.out.println(AnsiFormatter.format(s, background));
    }

    public static void println(String s, BackgroundColour background, Attribute... attributes) {
        System.out.println(AnsiFormatter.format(s, background, attributes));
    }

    public static void println(String s, Attribute... attributes) {
        System.out.println(AnsiFormatter.format(s, attributes));
    }
}
