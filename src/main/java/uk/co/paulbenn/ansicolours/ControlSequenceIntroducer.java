package uk.co.paulbenn.ansicolours;

public abstract class ControlSequenceIntroducer extends EscapeSequence {

    private static final char LEFT_SQUARE_BRACKET = '\u005B'; // = '[';

    private final char[] parameters;
    private final char terminator;

    protected ControlSequenceIntroducer(ControlSequenceIntroducerBuilder<?> builder) {
        super(builder);
        this.parameters = builder.parameters;
        this.terminator = terminator();
    }

    @Override
    protected char secondByte() {
        return LEFT_SQUARE_BRACKET;
    }

    protected abstract char terminator();

    @Override
    public String toString() {
        char[] chars = new char[parameters.length + 3];

        char[] superToString = super.toString().toCharArray();

        System.arraycopy(superToString, 0, chars, 0, 2);

        System.arraycopy(parameters, 0, chars, 2, parameters.length);

        chars[chars.length - 1] = terminator;

        return String.valueOf(chars);
    }

    protected abstract static class ControlSequenceIntroducerBuilder<T extends ControlSequenceIntroducerBuilder<T>>
        extends EscapeSequenceBuilder<T> {

        private char[] parameters;

        protected T setParameters(char[] parameters) {
            this.parameters = parameters;
            return getThis();
        }
    }
}
