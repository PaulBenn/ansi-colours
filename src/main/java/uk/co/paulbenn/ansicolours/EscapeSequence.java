package uk.co.paulbenn.ansicolours;

public abstract class EscapeSequence {

    private static final char ESC = '\u001B';

    protected EscapeSequence(EscapeSequenceBuilder<?> builder) {

    }

    protected abstract char secondByte();

    @Override
    public String toString() {
        char[] chars = new char[2];
        chars[0] = ESC;
        chars[1] = secondByte();

        return String.valueOf(chars);
    }

    protected abstract static class EscapeSequenceBuilder<T extends EscapeSequenceBuilder<T>> {

        protected abstract T getThis();
    }
}
