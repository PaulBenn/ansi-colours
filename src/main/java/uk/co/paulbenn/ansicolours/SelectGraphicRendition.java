package uk.co.paulbenn.ansicolours;

import uk.co.paulbenn.ansicolours.code.Attribute;
import uk.co.paulbenn.ansicolours.code.BackgroundColour;
import uk.co.paulbenn.ansicolours.code.ForegroundColour;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SelectGraphicRendition extends ControlSequenceIntroducer {

    static final SelectGraphicRendition RESET = SelectGraphicRendition.builder()
        .setAttributes(new Attribute[]{Attribute.RESET})
        .build();

    private SelectGraphicRendition(Builder builder) {
        super(builder);
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    protected char terminator() {
        return 'm'; // hard-coded terminator as per ECMA-48
    }

    protected static class Builder extends ControlSequenceIntroducerBuilder<Builder> {
        private ForegroundColour foregroundColour;
        private BackgroundColour backgroundColour;
        private Attribute[] attributes;

        Builder setForegroundColour(ForegroundColour colour) {
            this.foregroundColour = colour;
            return this;
        }

        Builder setBackgroundColour(BackgroundColour colour) {
            this.backgroundColour = colour;
            return this;
        }

        Builder setAttributes(Attribute[] attributes) {
            this.attributes = attributes;
            return this;
        }

        SelectGraphicRendition build() {
            List<Integer> parameters = new ArrayList<>();

            if (this.attributes != null) {
                Arrays.stream(attributes).forEach(a -> parameters.add(a.getCode()));
            } else {
                parameters.add(Attribute.RESET.getCode());
            }

            if (this.backgroundColour != null) {
                parameters.add(this.backgroundColour.getCode());
            }

            if (this.foregroundColour != null) {
                parameters.add(this.foregroundColour.getCode());
            }

            String codes = parameters.stream()
                .map(i -> Integer.toString(i))
                .collect(Collectors.joining(";"));

            return new SelectGraphicRendition(this.setParameters(codes.toCharArray()));
        }

        @Override
        protected Builder getThis() {
            return this;
        }
    }

}
