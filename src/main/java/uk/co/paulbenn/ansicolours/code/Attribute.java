package uk.co.paulbenn.ansicolours.code;

public enum Attribute {
    RESET(0),
    BOLD(1),
    UNDERLINE(4),
    REVERSE(7);

    private final int code;

    Attribute(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
