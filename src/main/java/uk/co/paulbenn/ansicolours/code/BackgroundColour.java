package uk.co.paulbenn.ansicolours.code;

public enum BackgroundColour {
    BLACK(40),
    RED(41),
    GREEN(42),
    YELLOW(43),
    BLUE(44),
    MAGENTA(45),
    CYAN(46),
    WHITE(47);

    private final int code;

    BackgroundColour(int code) {

        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
