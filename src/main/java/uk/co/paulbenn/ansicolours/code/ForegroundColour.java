package uk.co.paulbenn.ansicolours.code;

public enum ForegroundColour {
    BLACK(30),
    RED(31),
    GREEN(32),
    YELLOW(33),
    BLUE(34),
    MAGENTA(35),
    CYAN(36),
    WHITE(37);

    private final int code;

    ForegroundColour(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
