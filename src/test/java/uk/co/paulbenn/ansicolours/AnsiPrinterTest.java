package uk.co.paulbenn.ansicolours;

import org.junit.jupiter.api.Test;
import uk.co.paulbenn.ansicolours.code.Attribute;
import uk.co.paulbenn.ansicolours.code.BackgroundColour;
import uk.co.paulbenn.ansicolours.code.ForegroundColour;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class AnsiPrinterTest {

    @Test
    void exampleUsage() {
        assertDoesNotThrow(
            () -> AnsiPrinter.println("Hello, ANSI!", ForegroundColour.GREEN, BackgroundColour.BLACK, Attribute.BOLD)
        );
    }
}
